﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AllyController : AIController
{

    // Start is called before the first frame update
    void Start()
    {
        mainTarget = GameObject.Find("TeamTarget");
        mainTargetLocation = mainTarget.transform.localPosition;
        agent.SetDestination(mainTargetLocation);
    }

    // Update is called once per frame
    void Update()
    {
        GameObject[] oppArray = GameObject.FindGameObjectsWithTag("Enemy");
        agent.SetDestination(AIUpdate(oppArray));
    }
}
