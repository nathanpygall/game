﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIController : MonoBehaviour
{
    //yo

    public NavMeshAgent agent;
    public GameObject mainTarget;
    public Vector3 mainTargetLocation;
    public float lookRadius = 100f;


    public bool inCombat;
    public bool hasTarget;
    public GameObject currentTarget;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }

    public void FaceTarget()
    {
        Vector3 direction = (currentTarget.gameObject.transform.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

    public Vector3 AIUpdate(GameObject[] oppArray)
    {
        if (!hasTarget)//no enemy target
        {
            if (oppArray.Length > 0) //if there is an opp
            {
                foreach (var opp in oppArray) //checks each opp
                {
                    float distance = Vector3.Distance(opp.gameObject.transform.position, transform.position); //distance between x and opposition
                    if (distance <= lookRadius) //within aggro range
                    {
                        hasTarget = true; //found a target
                        currentTarget = opp; //first target in range
                        return currentTarget.gameObject.transform.position;
                    }
                }
            }
            return mainTargetLocation; //keep going towards opp base
        }
        else
        {
            if (hasTarget && Vector3.Distance(currentTarget.gameObject.transform.position, transform.position) <= lookRadius)//has a target within aggro range
            {
                if (Vector3.Distance(currentTarget.gameObject.transform.position, transform.position) <= agent.stoppingDistance) //if close enough to hit
                {
                    inCombat = true; //fighting target
                    FaceTarget(); //stays facing towards the target
                }
                return currentTarget.gameObject.transform.position; //go towards the current position of the opp
            }
            else if (hasTarget && Vector3.Distance(currentTarget.gameObject.transform.position, transform.position) >= lookRadius)//has a target which has gone outside of aggro range
            {
                hasTarget = false; //doesnt have a target anymore
                currentTarget = null;
                return mainTargetLocation; //keep going towards opp base
            }
            else
            {
                return mainTargetLocation; //catchall to ensure the ai has somewhere to go if there is any issues
            }
        }

    }
}

