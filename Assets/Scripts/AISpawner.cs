﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AISpawner : MonoBehaviour 
{
    // Reference to the Prefab. Drag a Prefab into this field in the Inspector.
    public GameObject ally;
    public GameObject enemy;
    
    public void AllySpawn()
    {
        int xPos = Random.Range(-99, 99);
        int yPos = Random.Range(-99, -85);
        // Instantiate at position (0, 0, 0) and zero rotation.
        Instantiate(ally, new Vector3(xPos, 1, yPos), Quaternion.identity);
    }

    public void EnemySpawn()
    {
        int xPos = Random.Range(-99, 99);
        int yPos = Random.Range(285, 299);
        // Instantiate at position (0, 0, 0) and zero rotation.
        Instantiate(enemy, new Vector3(xPos, 1, yPos), Quaternion.identity);
    }


}
