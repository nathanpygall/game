﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : AIController
{
    readonly string targetName = "EnemyTarget";

    // Start is called before the first frame update
    void Start()
    {
        mainTarget = GameObject.Find("EnemyTarget");
        mainTargetLocation = mainTarget.transform.localPosition;
        agent.SetDestination(mainTargetLocation);
    }

    // Update is called once per frame
    void Update()
    {
        GameObject[] oppArray = GameObject.FindGameObjectsWithTag("Ally"); //tag of the opp to this ai
        agent.SetDestination(AIUpdate(oppArray));
    }
}
