﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public float radius = 3f;
    public GameObject player;

    private void OnTriggerEnter(Collider other)
    {
        if (Input.GetButtonDown("Interact"))
        {
            Interact();
        }
    }

    public virtual void Interact()
    {
        //made to be overriden
        Debug.Log("Interacting");
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
